title: Nous écrire
faq: Questions fréquentes
menu:
  help:
  - J’ai besoin d’aide ou je rencontre un bug
  - sur l’un des services Framasoft…
  - sur…
  - un autre service Framasoft
  participate:
  - J’ai des idées, je veux participer,
  - proposer des améliorations, corriger des bugs…
  - proposer des améliorations, corriger des bugs pour…
  - un autre service Framasoft
  support:
  - Je soutiens financièrement Framasoft
  - mais…
  partnership:
  - J’envisage un partenariat avec vous
  - que peut-on faire ensemble ?
  - au sujet de…
  - ou autre chose
  event:
  - J’aimerais inviter un·e intervenant·e
  - pour un événement, une conférence, une table ronde…
  general:
  - Je souhaite discuter avec l’association
  - au sujet d’une interview, un emploi, un événement du libre à relayer…
breadcrumb:
  - Prérequis
  - Service concerné
  - <abbr title="Questions fréquentes">FAQ</abbr>
  - Formulaire
intro: |-
  ## Prérequis avant de nous contacter

  Pour rappel, @:color.soft **est une association loi 1901 de @:people.members membres dont @:people.employees
  salarié·es** : les réponses à vos questions sont assurées par un seul
  de ces salarié·es (épaulé par le reste de l’association).

  De manière générale, nous ne sommes pas incollables sur tous les sujets qui
  relèvent du libre ou du numérique. Selon la nature de votre demande, **il est peut-être préférable
  de solliciter la communauté avant de nous contacter**.

  Plusieurs outils sont à votre disposition :

  - [notre forum](@:link.colibri), notamment la
    [section « Entraide »](@:link.colibri/c/entraide/53)
  - ou les réseaux sociaux

  Vérifiez également que la réponse ne figure pas déjà dans notre
  [documentation](@:link.docs) ou la [FAQ](@:link.contact/faq).

  Avant d’accéder à notre formulaire de contact, **merci de cliquer ci-dessous
  sur la catégorie concernée** afin que nous puissions vous répondre le plus
  efficacement possible.
help:
  title: 'Besoin d’aide ? Sélectionnez ci-dessous le projet concerné :'
participate:
  title: 'Pour contribuer, merci de sélectionnez ci-dessous le projet concerné :'
  dev: |-
    ### Bugs, fonctionnalités et développement

    Avant tout, sachez que **@:color.soft fait peu de développement**.
    À quelques exceptions près, nous ne faisons qu’adapter légèrement des logiciels
    existants pour les proposer au public.

    Pour soumettre une idée d’amélioration, il est donc important de savoir
    si vos contributions concernent les adaptations réalisées spécifiquement
    pour les projets @:color.soft
    (ci-dessous notre dépôt est indiqué d’un {git}) ou le logiciel original
    ({source}).

    Il sera plus efficace de signaler un bug ou proposer des améliorations au
    code, si vous le faites depuis les sites de développements respectifs.
    À plus forte raison si votre idée a déjà été proposée par d’autres personnes.

    Si vous ne savez pas comment créer une issue sur Github/Gitlab, nous pouvons
    [le faire à votre place]({link}).
  other: |-
    ### Graphisme, relectures, traductions, etc.
form:
  title: Formulaire de contact
  required: obligatoire
  optional: facultatif
  name: Nom, prénom ou pseudo
  email: E-mail
  subject: Sujet
  concerne: Votre demande concerne
  message: Message
  file: Fichier ou capture écran
  fileHelp: 'Une pièce jointe n’est pas obligatoire mais peut être très utile. Voici
    la liste des extensions autorisées :'
  submit: Envoyer
  sending: Envoi en cours…
  error_empty: Ce champ doit être renseigné.
  error_email: Vous devez saisir une adresse email valide.
alert:
  email: Pensez à **vérifier votre adresse email**. Si elle est fausse, nous
    ne pourrons pas vous répondre !
  sent: Le message a bien été envoyé.<br />
    Vous allez recevoir un mail automatique de confirmation.<br />
    Pensez à **vérifier** qu’il ne se trouve pas dans **votre dossier spam**.
  failed: Une erreur est survenue. Le message n’a pu être envoyé.<br />
    Veuillez réessayer ultérieurement.
  libre: <b class="badge badge-warning">Attention</b> Il ne sera donné **aucune
    réponse aux questions concernant le fonctionnement de logiciels libres**.
    Nous ne connaissons pas tous les logiciels qui figurent dans notre annuaire.
    Pour cela, merci d’utiliser [nos forums](@:link.colibri) ou
    les réseaux sociaux.
  pad: N’oubliez pas de **nous donner l’adresse complète du pad** qui vous pose
    problème dans votre message.
  calc: N’oubliez pas de **nous donner l’adresse du calc** qui vous pose problème
    dans votre message.
  date: N’oubliez pas de **nous donner l’adresse du sondage** qui vous pose problème
    dans votre message.
  memo: N’oubliez pas de **nous donner l’adresse du memo** qui vous pose problème
    dans votre message.
  forms: N’oubliez pas de **nous donner l’adresse du formulaire** qui vous pose
    problème dans votre message.
  board: N’oubliez pas de **nous donner l’adresse de votre espace** @:color.board
    ainsi que **votre identifiant** dans votre message.
  drive: N’oubliez pas de **nous donner l’identifiant de votre compte** dans
    votre message.
  agenda: N’oubliez pas de **nous donner l’identifiant de votre compte** dans
    votre message.
  listes: N’oubliez pas de **nous donner le nom de votre liste** dans votre message.
  site: N’oubliez pas de **nous donner l’adresse de votre site** dans votre message.
  wiki: N’oubliez pas de **nous donner l’adresse de votre wiki** dans votre message.
  team: N’oubliez pas de **nous donner votre identifiant** ainsi que **les équipes
    où vous êtes inscrits** dans votre message.
  newsletter: Si vous souhaitez vous désabonner de notre lettre d’information,
    inutile de nous contacter. Cliquez simplement sur ce bouton.
  referrer: Votre navigateur web indique que vous venez de consulter le service
  mdp: Ne nous donnez **jamais vos mots de passe**, nous n’en avons pas besoin.
    N’oubliez pas qu’[un email est une carte postale sans
    enveloppe](https://framablog.org/2016/11/23/10-trucs-que-jignorais-sur-internet-et-mon-ordi-avant-de-my-interesser).
  kindness: '**Votre message est-il bienveillant ?**<br />
    Framasoft est [une association à taille humaine vivant exclusivement
    des dons](@:link.soutenir).<br />
    Améliorer nos services et vous aider requiert **du temps et de l’énergie
    bénévole**.<br />
    Merci d’en tenir compte lorsque vous rédigez votre message.'
  inclusive: |-
    L’usage de l’écriture inclusive¹ **n’est pas un acte accidentel**.
    Depuis nos premières pratiques en 2015, vous pensez bien que
    **nous avons déjà réfléchi** à ses implications.
    Il est donc **inutile de contacter le support** à ce sujet.

    La position de Framasoft est publique et rien ne justifie actuellement
    qu’elle soit révisée ou que nous dépensions davantage d’énergie à nous expliquer.
    Vous trouverez la réponse à vos questions :
    - [dans la FAQ ici même](https://contact.framasoft.org/fr/faq/#blog-inclusif),
    - [dans cet article du @:color.blog](https://framablog.org/2018/08/13/ecriture-du-blog-nous-ne-transigerons-pas-sur-les-libertes/)
    - et sur [le forum (plus spécifiquement concernant Framadate)](https://framacolibri.org/t/possibilite-de-ne-pas-avoir-lecriture-inclusive-dans-framadate/12971/14)

    ¹ essentiellement [les abréviations à l’aide « du point médian »](@:(baseurl)img/point-median.png)
    puisque, au fond, les autres formes d’écriture (mots épicènes, double flexion, mots-valises…)
    ne suscitent aucune réaction.
reminder:
  date: |-
    **Rappel :** @:color.date permet à tout le monde de créer des sondages.
    **Mais @:txt.soft n’est pas responsable de leur contenu ni des données collectées**.

    Si votre intention est de contacter l’auteur·rice d’un sondage, **merci
    d’utiliser les commentaires sous le @:color.date** ou de vous
    adresser à la personne qui vous a transmis le lien du sondage.
  forms: |-
    **Rappel :** @:color.forms pernet à tout le monde de créer des formulaires.
    **Mais @:txt.soft n’est pas responsable de leur contenu ni des données collectées**.

    Si votre intention est de contacter l’auteur·rice d’un formulaire, **merci
    d’utiliser le lien de contact en bas de la page du @:color.forms** ou de vous
    adresser à la personne qui vous a transmis le lien du formulaire.
concerne:
- Annonce d’un événement libre
- Invitation à un événement
- Partenariat
- Contact presse
- Newsletter
- Questions sur l’association
- 'Prestation / Emploi : réponse ou candidature spontanée'
