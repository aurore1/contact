/* eslint-disable global-require */
/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');

const data = yaml.load(fs.readFileSync('./src/data/main.yml', { encoding: 'utf-8' }));
if (!('config' in data)) { data.config = {}; }
if (!('wiki' in data.config)) { data.config.wiki = {}; }

const mode = {
  dev: process.env.NODE_ENV === 'development',
  preview: /^pages/.test(process.env.CI_JOB_NAME) && process.env.NODE_ENV === 'production',
  prod: !/^pages/.test(process.env.CI_JOB_NAME) && process.env.NODE_ENV === 'production',
};

const env = {
  assets: [],
  basepath: mode.preview
    ? `${process.env.CI_PAGES_URL.replace(/^.*\/\/[^/]+/, '')}/`
    : '/',
  commons: {
    available: [],
    enabled: [],
  },
  date: fs.statSync('./src').mtime,
  dist: path.resolve(__dirname, '../dist'),
  img: {
    assets: './public/img/',
    dist: './dist/img/',
  },
  framanav: ('framanav' in data.config) ? data.config.framanav : false,
  meta: data.meta,
  mode: mode.preview ? 'preview' : process.env.NODE_ENV,
  routes: [],
  src: path.resolve(__dirname, '../src'),
  translations: {
    all: yaml.load(fs.readFileSync('./commons/translations/lang.yml', { encoding: 'utf-8' })),
    available: [],
    default: ('defaultLang' in data.config) ? data.config.defaultLang : 'en',
    translate: ('translate' in data.config) ? data.config.translate : '',
  },
  url: mode.preview
    ? process.env.CI_PAGES_URL.replace(/\/?$/, '/').replace(/^http:/, 'https:')
    : 'http://localhost:8080/',
  views: [],
  wiki: {
    api: ('api' in data.config.wiki) ? data.config.wiki.api : '',
    base: ('base' in data.config.wiki) ? data.config.wiki.base : '',
  },
};

if (mode.prod && env.meta.canonical !== undefined) {
  env.url = env.meta.canonical.replace(/\/?$/, '/');
}

// Custom base
for (let i = 0; i < process.argv.length; i += 1) {
  if (process.argv[i].indexOf('--base=') > -1) {
    env.basepath = `/${process.argv[i].split('=')[1]}/`;
  }
}
env.img.dist = `./dist${env.basepath}img/`;
env.base = env.basepath.replace(/\/$/, '').replace(/^\//, '');

// Get assets list
fs.readdirSync('./public').forEach((file) => {
  env.assets.push({ from: path.resolve(__dirname, `../public/${file}`), to: file });
});

// Get translations list
fs.readdirSync('./src/translations')
  .forEach((lg) => {
    if (env.translations[lg] === undefined) {
      env.translations[lg] = [];
    }
    fs.readdirSync(`./src/translations/${lg}`)
      .forEach((file) => {
        if (/[A-Za-z0-9-]+\.ya?ml/.test(file)) {
          env.translations[lg].push(file.replace(/([A-Za-z0-9-]+)\.ya?ml/, '$1'));
        }
      });
  });

env.translations.available = Object
  .keys(env.translations.all)
  .filter(lg => Object.keys(env.translations).includes(lg)
    && (env.translations[lg].includes('main')));

// Get views list
fs.readdirSync('./src/views')
  .forEach((file) => {
    if (/[A-Za-z0-9-]+\.vue/.test(file)) {
      env.views.push(file.replace(/([A-Za-z0-9-]+)\.vue/, '$1'));
    }
  });

// Get commons data list
fs.readdirSync('./commons/data')
  .forEach((file) => {
    if (/[A-Za-z0-9-]+\.ya?ml/.test(file)) {
      env.commons.available.push(file.replace(/([A-Za-z0-9-]+)\.ya?ml/, '$1'));
    }
  });
/* Take all data commons if enabled list is empty */
[env.commons.enabled] = ('commons' in data.config && data.config.commons.length)
  ? [data.config.commons]
  : [env.commons.available];

for (let j = 0; j < env.views.length; j += 1) {
  env.routes.push(`${env.basepath}${env.views[j].toLowerCase().replace('home', '')}`);
  // Localized routes
  for (let i = 0; i < env.translations.available.length; i += 1) {
    env.routes.push(`${env.basepath}${env.translations.available[i]}${
      env.views[j].toLowerCase().replace(/^/, '/').replace('/home', '')}`);
  }
}

/* --- Wiki ----------------------------------------------------------------- */
if (env.wiki.api !== '') {
  env.assets.push(
    { from: path.resolve(__dirname, '../public/wiki.json'), to: 'wiki.json' },
    { from: path.resolve(__dirname, '../public/wiki/'), to: 'wiki/' },
  );

  // Import json
  const json = require('../public/wiki.json');
  // Get wiki data
  const wiki = require('../src/wiki')(env, json);
  // RSS
  require('./feed')(env, wiki);
} else {
  fs.writeFileSync(path.resolve(__dirname, '../public/wiki.json'), '[]');
}

/* --- Symlinks-------------------------------------------------------------- */
require('./symlinks')(env);

module.exports = env;
